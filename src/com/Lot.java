package com;

public class Lot {
    private int id;
    private String name;
    private int sellerId;

    public Lot(int id, String name, int sellerId){
        this.id = id;
        this.name = name;
        this.sellerId = sellerId;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }
    public int getId(){
        return id;
    }
    public String getName(){
        return name;
    }
    public int getSellerId(){
        return sellerId;
    }
    @Override
    public String toString(){
        return getClass().getSimpleName() + "id: " +id + " Name: " +name+ " sellerId : " + sellerId;
    }
}