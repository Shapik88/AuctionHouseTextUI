package com;

import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.sql.*;
import java.util.Scanner;

public class AddBid {private  final String URL = "jdbc:mysql://localhost:3306/auction_house_text_ui";
    private  final String USERNAME = "root";
    private  final String PASSWORD = "root";

       public void addBid() throws SQLException {

        Scanner scanner = new Scanner(System.in);
        Connection connection;
        PreparedStatement preparedStatement;
        try {
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);

            String query = " insert into bid (lot_id, buyer_id, seller_id, first_bet, second_bet)"
                    + " values (?, ?, ?, ?, ?)";

            System.out.println("lot id: ");
            int lotId = scanner.nextInt();
            System.out.println("buyer id: ");
            int buyerId = scanner.nextInt();
            System.out.println("seller id: ");
            int sellerId = scanner.nextInt();
            System.out.println("first bet: ");
            int firstBet = scanner.nextInt();
            System.out.println("second bet: ");
            int secondBet = scanner.nextInt();

            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt (1, lotId);
            preparedStatement.setInt (2, buyerId);
            preparedStatement.setInt   (3, sellerId);
            preparedStatement.setInt(4, firstBet);
            preparedStatement.setInt(5, secondBet);

            preparedStatement.execute();

            connection.close();
        }
        catch (Exception e)
        {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }
}