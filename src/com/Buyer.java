package com;


public class Buyer { //покупатель

    private int id;
    private String name;

    public Buyer(int id, String name){
        this.id = id;
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getId(){
        return id;
    }
    public String getName(){
        return name;
    }
    @Override
    public String toString(){
        return getClass().getSimpleName() + "id: " +id + " name: "+ name + " ";
    }
}