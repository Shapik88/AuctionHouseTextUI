package com;

import com.mysql.fabric.jdbc.FabricMySQLDriver;
import java.sql.*;
import java.util.Scanner;

public class AddSeller {
    private  final String URL = "jdbc:mysql://localhost:3306/auction_house_text_ui";
    private  final String USERNAME = "root";
    private  final String PASSWORD = "root";

    public void addSeller() throws SQLException {

        Scanner scanner = new Scanner(System.in);
        Connection connection;
        PreparedStatement preparedStatement;
        try {
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            String query = " insert into seller (seller_first_name, seller_last_name, seller_name_lot, seller_license_number)"
                    + " values (?, ?, ?, ?)";

            System.out.println("seller first name: ");
            String sellerFirstName = scanner.nextLine();
            System.out.println("seller last name: ");
            String sellerLastName = scanner.nextLine();
            System.out.println("seller name lot: ");
            String sellerNameLot = scanner.nextLine();
            System.out.println("seller license number:");
            String sellerLicenseNumber = scanner.nextLine();

            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString (1, sellerFirstName);
            preparedStatement.setString (2, sellerLastName);
            preparedStatement.setString (3, sellerNameLot);
            preparedStatement.setString   (4, sellerLicenseNumber);

            preparedStatement.execute();

            connection.close();
        }
        catch (Exception e)
        {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }
}

