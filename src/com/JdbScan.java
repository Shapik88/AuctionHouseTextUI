package com;

import java.sql.*;
import java.util.Scanner;

public class JdbScan {
    AddSeller addSeller = new AddSeller();
    AddBuyer addBuyer = new AddBuyer();
    AddBid addBid = new AddBid();
    AddLot addLot = new AddLot();

    Scanner scanner = new Scanner(System.in);
    public void menu(){
        System.out.println("если вы хотите добавить продавца нажмите 1");
        System.out.println("если вы хотите добавить покупателя нажмите 2");
        System.out.println("если вы хотите добавить товар нажмите 3");
        System.out.println("если вы хотите сделать ставку нажмите 4");
        System.out.println("если вы хотите выйти нажмите 5");
    }
    public void switchMenu() throws SQLException {
        int selectionOut = scanner.nextInt();
        while(selectionOut != 5) {

            int selection = scanner.nextInt();
            switch (selection) {
                case 1:
                    addSeller.addSeller();
                    menu();
                    break;
                case 2:
                    addBuyer.addBuyer();
                    menu();
                    break;
                case 3:
                    addLot.addLot();
                    menu();
                    break;
                case 4:
                    addBid.addBid();
                    menu();
                    break;
                default:
                    System.out.println("Error");

            }
        }
    }
}




