package com;

public class Seller { //продавец
    private int id;
    private String firstName;
    private String lastName;
    private String nameLot;

    public Seller(int id, String firstName, String lastName, String nameLot){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.nameLot = nameLot;

    }
    public int getId(){
        return id;
    }
    public String getFirstName(){
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getNameLot() {
        return nameLot;
    }
    @Override
    public String toString(){
        return getClass().getSimpleName() + " " +id + " firstName: " +firstName+ " lastName: " + lastName+ " nameLot: "+ nameLot;
    }

}