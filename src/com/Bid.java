package com;

public class Bid { private int id;

    public int lotId;
    public int sellerId;
    public int buyerId;
    public int firstBet;
    public int secondBet;

    public Bid(int id, int lotId, int buyerId, int sellerId, int firstBet, int secondBet){
        this.id = id;
        this.sellerId = sellerId;
        this.lotId = lotId;
        this.buyerId = buyerId;
        this.firstBet = firstBet;
        this.secondBet = secondBet;

    }

    public int getId(){
        return id;
    }
    public int getsellerId(){
        return sellerId;
    }
    public int getLotId(){
        return lotId;
    }
    public int getBuyerId() {
        return buyerId;
    }
    public int getFirstBet() {
        return firstBet;
    }
    public int getSecondBet() {
        return secondBet;
    }
    @Override
    public String toString(){
        return getClass().getSimpleName() + "id: " +id + " sellerId: " +sellerId+ " lotId: " + lotId+ " buyerId: "+ buyerId+
                " firstBet: "+firstBet+ " secondBet:"+ secondBet;

    }

}