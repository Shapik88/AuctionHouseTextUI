package com;

import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.sql.*;
import java.util.Scanner;

public class AddBuyer { private  final String URL = "jdbc:mysql://localhost:3306/auction_house_text_ui";
    private  final String USERNAME = "root";
    private  final String PASSWORD = "root";

    public void addBuyer() throws SQLException {

        Scanner scanner = new Scanner(System.in);
        Connection connection;
        PreparedStatement preparedStatement;
        try {
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            String query = " insert into buyer (buyer_name, buyer_license_number)"
                    + " values (?, ?)";

            System.out.println("buyer name: ");
            String buyerName = scanner.nextLine();
            System.out.println("buyer license number: ");
            String buyerLicenseNumber = scanner.nextLine();

            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString (1, buyerName);
            preparedStatement.setString (2, buyerLicenseNumber);

            preparedStatement.execute();

            connection.close();
        }
        catch (Exception e)
        {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }
}