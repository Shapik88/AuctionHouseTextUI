package com;

import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.sql.*;
import java.util.Scanner;

public class AddLot {
    private  final String URL = "jdbc:mysql://localhost:3306/auction_house_text_ui";
    private  final String USERNAME = "root";
    private  final String PASSWORD = "root";

    public void addLot() throws SQLException {

        Scanner scanner = new Scanner(System.in);
        Connection connection;
        PreparedStatement preparedStatement;
        try {
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            String query = " insert into lot (name_lot, seller_id)"
                    + " values (?, ?)";

            System.out.println("name lot: ");
            String nameLot = scanner.nextLine();
            System.out.println("seller id: ");
            String sellerId = scanner.nextLine();

            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString (1, nameLot);
            preparedStatement.setString (2, sellerId);

            preparedStatement.execute();

            connection.close();
        }
        catch (Exception e)
        {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }
}

