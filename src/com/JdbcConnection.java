package com;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcConnection {

    private  final String URL = "jdbc:mysql://localhost:3306/auction_house_text_ui";
    private  final String USERNAME = "root";
    private  final String PASSWORD = "root";
    private Connection connection;
    public Connection getConnection() {
        return connection;
    }
    public JdbcConnection(){
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


}
